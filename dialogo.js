"use strict";

class Dialogo {
  constructor() {
    // Obtenemos los objetos relevantes del documento
    this.overlay = document.getElementById("overlay");
    this.btn = document.getElementById("botonDialogo");
    this.span = document.getElementsByClassName("botonCerrar")[0];

    // le asignamos a cada boton una funcion qie se llamara al presionar
    this.btn.addEventListener("click", this.mostrar().bind(this));
    this.span.addEventListener("click", this.ocultar().bind(this));
  }

  mostrar() {
    let f = function() {
      this.overlay.style.display = "block";
    };

    return f;
  }

  ocultar() {
    let f = function() {
      this.overlay.style.display = "none";
    };

    return f;
  }
}

export { Dialogo };
