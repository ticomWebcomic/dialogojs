# Welcome to Glitch

Un ejemplo de como crear un dialogo de notificacion usando solo JavaScript

Find out more [about Glitch](https://glitch.com/about).

## Your Project

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

La pagina principal

### ← style.css

El estilo general

### ← dialogo.css

Estilo css para el dialogo

### ← script.js

Script principal

### ← dialogo.js

Clase para manejar el dialogo

### ← assets

Imagenes varias

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
